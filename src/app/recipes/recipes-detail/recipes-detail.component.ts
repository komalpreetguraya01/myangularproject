import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.modal';
import { RecipeService } from '../recipe.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-recipes-detail',
  templateUrl: './recipes-detail.component.html',
  styleUrls: ['./recipes-detail.component.css']
})
export class RecipesDetailComponent implements OnInit {

  recipeSelected: Recipe;
  id: number;
  constructor(private recipeService: RecipeService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.params.
    subscribe(
      (params: Params) => {
          this.id = +params['id'];
          this.recipeSelected = this.recipeService.getRecipeById(this.id);
      }
    )
  }

  addIngredientsToShoppingList(){
    this.recipeService.addIngredientsToShoppingList(this.recipeSelected.ingredients);
  }

  addEditShoppingList(){
      this.router.navigate(['../',this.id,'edit'], {relativeTo: this.route })
  }

  deleteShoppingList(){
    this.recipeService.deleteRecipe(this.id);
    this.router.navigate(['/recipes']);
  }
}
