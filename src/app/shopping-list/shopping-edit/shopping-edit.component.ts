import { Component, OnInit, ElementRef, ViewChild, EventEmitter, Output } from '@angular/core';

import { Ingredient } from '../../shared/ingredient.modal';
import { ShoppingListService } from '../shopping-list.service';
import { NgForm } from '@angular/forms';
import { Subscription, ArgumentOutOfRangeError } from 'rxjs';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {

  subscription: Subscription;
  @ViewChild('f') shoppingForm: NgForm;
  editMode = false;
  editedItemIndex : number;
  editedItem : Ingredient;

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
      this.subscription =  this.shoppingListService.editIngredients.subscribe(
        (index: number) => {
          this.editedItemIndex = index;
          this.editMode = true;
          this.editedItem = this.shoppingListService.getIngredientByIndex(index);
          this.shoppingForm.setValue({
            name: this.editedItem.name,
            amount: this.editedItem.amount
          });
        }
      ); 
  }
  onSubmit(){
    const ingredient = new Ingredient(this.shoppingForm.value.name, this.shoppingForm.value.amount ) ;
    if(!this.editMode){
      this.shoppingListService.addIngredient(ingredient);
    }
    else{
      this.shoppingListService.updateIngredient(this.editedItemIndex,ingredient);
    }
    this.editMode = false;
    this.shoppingForm.reset();
  }

  onDelete(){
    this.shoppingListService.deleteIngredient(this.editedItemIndex);
    this.editMode = false;
    this.onClear();
  }
  onClear(){
      this.shoppingForm.reset();
  }
}
