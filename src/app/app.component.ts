import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'myangularproject';

  ngOnInit(){
    firebase.initializeApp({
      apiKey: "AIzaSyDVnSHubj8ztQeCne0IBI4LxlrkEA2DHfI",
      authDomain: "angular-project-ed677.firebaseapp.com"
    });
  }
}
